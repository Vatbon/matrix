#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define N 2048
#define M 10

int matmul(const float *ao,const float *bo, float *res){
	volatile float x,tmp;
	int i, j, r;
	float *a, *b;
	a = (float*)malloc(N*N*sizeof(float));
	b = (float*)malloc(N*N*sizeof(float));
	/*for (i = 0; i < N; i ++)
		for(j = 0; j < N; j++){
			a[N*i + j] = ao[N*i + j];
			b[N*i + j] = bo[N*j + i];
		}*/
	memcpy(a, ao, N*N*sizeof(float));
	memcpy(b, bo, N*N*sizeof(float));
	for (i = 0; i< N; i++)
		for(j=0;j<N;j++){
			tmp = b[N*i+j];
			b[N*i +j] = b[N*j +i];
			b[N*j +i] = tmp;
		}	
	for (i = 0; i < N; i ++)
		for(j = 0; j < N; j++){
			x = 0;
			for (r = 0; r < N; r++){
				x += a[N*i + r] * b[N*r + j];
			}
			res[N*i + j] = x;
		}
	return 0;
}

float matsum(const float *ao,const float *bo, float *res){
	int i, j;
	float *a, *b;
	/*a = (float*)malloc(N*N*sizeof(float));
	b = (float*)malloc(N*N*sizeof(float));
	memcpy(a, ao, N*N*sizeof(float));
	memcpy(a, ao, N*N*sizeof(float));
	for (i = 0; i < N; i++)
		for(j = 0; j < N; j++){
			a[N*i + j] = ao[N*i + j];
			b[N*i + j] = bo[N*i + j];
		}*/
	for (i = 0; i < N; i ++)
		for(j = 0; j < N; j++)
			res[N*i + j] = ao[N*i + j] + bo[N*i + j];
	return 0;
}

int main(){
	srand(1);
	int i, j, z, m;
	m = M;
	float *A, *B, *AT, *I;
	float vs, hs;
	volatile float tvs, ths;
	float *R;
	A = (float*)malloc(N*N*sizeof(float));
	B = (float*)malloc(N*N*sizeof(float));
	AT = (float*)malloc(N*N*sizeof(float));
	I = (float*)malloc(N*N*sizeof(float));
	R = (float*)malloc(N*N*sizeof(float));
	for (i = 0; i < N; i++){
		for(j = 0; j < N; j++){
			if (i == j)
				I[N*i + j] = 1;
			else
				I[N*i + j] = 0;
			A[N*i + j] = (float)rand()/((float)(rand()+1));
		}
	}
	/*for (i = 0; i < N; i++){
		for(j = 0; j < N; j++){
			printf("%f ", A[N*i + j]);
		}
		printf("\n");
	}
	printf("\n");
	printf("{");
	for (i = 0; i < N; i++){
		printf("{");
		for(j = 0; j < N; j++){
			printf("%f,", A[N*i + j]);
		}
		printf("},");
	}
	printf("}\n");*/
	vs = -1; hs = -1;
	for (i = 0; i < N; i++){
		ths = 0; tvs =0;
		for(j = 0; j < N; j++){
			ths += A[N*i + j] > 0 ? A[N*i + j] : -A[N*i + j];
			tvs += A[N*j + i] > 0 ? A[N*j + i] : -A[N*j + i];
		}
		if (ths > hs)
			hs = ths;
		if (tvs > vs)
			vs = tvs;
	}
	printf("vs = %f\nhs = %f\n", vs, hs);
	for (i = 0; i < N; i++)
		for(j = 0; j < N; j++){
			B[N*i + j] =A[N*j + i] / (hs*vs);
		}
	matmul(B, A, R); // I + (-1)(BA);
	for (i = 0; i < N; i++)
		for(j = 0; j < N; j++){
			R[N*i + j] = R[N*i + j] * (-1);
		}
	matsum(I, R, R);	
	for (i = 0; i < N; i++)
		for(j = 0; j < N; j++){
			AT[N*i + j] = I[N*i + j];
		}

	for (i = 0; i < N; i++)
		for(j = 0; j < N; j++)
			I[N*i + j] = R[N*i + j];
	// all matrixes ready
	for (z = 1; z < M; z++){
		matsum(AT, R, AT);// (I + R + ...)
		matmul(R, I, R);
	}
	matmul(AT, B, AT);// (...)B
	/*for (i = 0; i < N; i++){
		for(j = 0; j < N; j++){
			printf("%f ", AT[N*i + j]);
		}
		printf("\n");
	}*/
	return 0;
}
