gcc main.c -O2 -o novec.exe 
gcc mainvec.cpp -msse -msse2 -O2 -o vec.exe
gcc mainblas.c -lblas -latlas -O2 -o blas.exe
echo "Time report" > time.txt
echo "Без векторизации" >> time.txt
time >> time.txt ./novec.exe >> time.txt
echo "Ручная векторизация" >> time.txt
time ./vec.exe >> time.txt
echo "Библиотека BLAS/ATLAS" >> time.txt
time ./blas.exe >> time.txt
