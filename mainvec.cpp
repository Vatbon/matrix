#include <xmmintrin.h>
#include <stdio.h>
#include <stdlib.h>
#define N 2048
#define M 10
void matsum(const float *A, const float *B, float *R){
	__m128 *AA, *BB, *RR;
	AA = (__m128*) A;
	BB = (__m128*) B;
	RR = (__m128*) R;
	volatile int i;
	for (i = 0; i < N*N/4; i++)
		RR[i] = _mm_add_ps(AA[i], BB[i]);
}

void matmul(float *A, float *B, float *R){
	volatile int i, j, z;
	__m128 *AA, *BB;
	__m128 s, p;
	AA = (__m128*) A;
	BB = (__m128*) B;
	for (i = 0; i< N; i++)
		for(j = 0; j < N; j++){
			s = _mm_setzero_ps();
			for(z = 0 ; z < N/4; z++){
				p = _mm_mul_ps(AA[i*N/4 + z], BB[j*N/4 + z]);
				s = _mm_add_ps(s, p);
			}
			p = _mm_movehl_ps(p, s);
			s = _mm_add_ps(s, p);
			p = _mm_shuffle_ps(s, s, 1);
			s = _mm_add_ss(s, p);
			_mm_store_ss((R+i*N + j), s);
		}
}

int main(){
	float *A, *B, *I, *R, *AR, *Z, *T;
	float ths, hs, tvs, vs;
	A = (float*)_mm_malloc(N*N*sizeof(float), 16);
	B = (float*)_mm_malloc(N*N*sizeof(float), 16);
	I = (float*)_mm_malloc(N*N*sizeof(float), 16);
	R = (float*)_mm_malloc(N*N*sizeof(float), 16);
	AR = (float*)_mm_malloc(N*N*sizeof(float), 16);
	Z = (float*)_mm_malloc(N*N*sizeof(float), 16);
	int i, j, z;
	for (i = 0; i < N; i++){
		for(j = 0; j < N; j++){
			if (i == j)
				I[N*i + j] = 1;
			else
				I[N*i + j] = 0;
			A[N*i + j] = (float)rand()/((float)(rand()+1));
		}
	}
	/*for (i = 0; i < N; i++){
		for(j = 0; j < N; j++){
			printf("%f ", A[i*N + j]);
		}
		printf("\n");
	}*/
/*	for (i = 0; i < N; i++){
		for(j = 0; j < N; j++){
			printf("%f ", AR[i*N + j]);
		}
		printf("\n");
	}
	for (i = 0; i < N; i++)
		for(j = 0; j < N; j++)
			I[i*N + j] = AR[j*N + i];

	for (i = 0; i < N; i++){
		for(j = 0; j < N; j++){
			printf("%f ", I[i*N + j]);
		}
		printf("\n");
	}
		printf("\n");
	matmul(AR, I, Z);
	for (i = 0; i < N; i++){
		for(j = 0; j < N; j++){
			printf("%f ", Z[i*N + j]);
		}
		printf("\n");
	}*/

	vs = -1; hs = -1;
	for (i = 0; i < N; i++){
		ths = 0; tvs =0;
		for(j = 0; j < N; j++){
			ths += A[N*i + j] > 0 ? A[N*i + j] : -A[N*i + j];
			tvs += A[N*j + i] > 0 ? A[N*j + i] : -A[N*j + i];
		}
		if (ths > hs)
			hs = ths;
		if (tvs > vs)
			vs = tvs;
	}
	printf("vs = %f\nhs = %f\n", vs, hs);
	for (i = 0; i < N; i++)
		for(j = 0; j < N; j++){
			B[N*i + j] =A[N*j + i] / (hs*vs);
			AR[N*i + j] =A[N*j + i];
			
		}
	matmul(B, AR, Z);// Z = AB B = A^T^T
	for (i = 0; i < N; i++)
		for(j = 0; j < N; j++){
			Z[N*i + j] = -Z[N*i + j];
		}
	// BA -> -BA
	matsum(I, Z, R);// R = I - BA;

	/*for (i = 0; i < N; i++)
	  for(j = 0; j < N; j++){
	  B[N*i + j] = B[N*j + i];
	  }
	  */
	for (i = 0; i < N; i++)
		for(j = 0; j < N; j++){
			Z[N*i + j] = R[N*j + i];
			AR[N*i + j] =I[N*i + j];
		}
	for (z = 0 ; z < M; z++){
		matsum(AR, R, AR);
		/*for (i = 0; i < N; i++){
			for(j = 0; j < N; j++){
				printf("%f ", R[i*N + j]);
			}
			printf("\n");
		}
		printf("--------------------------------\n");*/
		if (z + 1 < M){
			matmul(R, Z, I);// Z = R^T;
			for (i = 0; i < N; i++)
				for(j = 0; j < N; j++){
					R[N*i + j] = I[N*i + j];
				}
		}
	}
	for (i = 0; i < N; i++)
		for(j = 0; j < N; j++){
			Z[N*i + j] = B[N*j + i];
		}
	matmul(AR, Z, B);
	/*for (i = 0; i < N; i++)
		for(j = 0; j < N; j++){
			AR[N*i + j] = A[N*j + i];
		}
	matmul(B, AR, I);
	for (i = 0; i < N; i++){
		for(j = 0; j < N; j++){
			printf("%f ", I[i*N + j]);
		}
		printf("\n");
	}*/
	// B == result


	return 0;
}
