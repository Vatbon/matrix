#include <stdio.h>
#include <stdlib.h>
#include <cblas.h>  // заголовочный файл C-интерфейса библиотеки BLAS
#define N 2048
#define M 10
int main(){
	srand(1);
	float *A, *B, *R, *I, *Z, *RA, *RB;
	A = (float*)malloc(N*N*sizeof(float));
	B = (float*)malloc(N*N*sizeof(float));
	R = (float*)malloc(N*N*sizeof(float));
	I = (float*)malloc(N*N*sizeof(float));
	Z = (float*)malloc(N*N*sizeof(float));
	RA = (float*)malloc(N*N*sizeof(float));
	RB = (float*)malloc(N*N*sizeof(float));
	int i, j, z; float ths, tvs, hs, vs;
	for (i = 0; i < N; i++){
		for(j = 0; j < N; j++){
			if (i == j)
				I[N*i + j] = 1;
			else
				I[N*i + j] = 0;
			A[N*i + j] = (float)rand()/((float)(rand()+1));
		}
	}

	printf("{");
	/*for (i = 0; i < N; i++){
		printf("{");
		for(j = 0; j < N; j++){
			printf("%f,", A[N*i + j]);
		}
		printf("},");
	}*/
		printf("}\n");
	/*for (i = 0; i < N; i++){
		for(j = 0; j < N; j++){
			printf("%f ", A[N*i + j]);
		}
		printf("\n");
	}*/
	printf("\n");
	vs = -1; hs = -1;
	for (i = 0; i < N; i++){
		ths = 0; tvs =0;
		for(j = 0; j < N; j++){
			ths += A[N*i + j] > 0 ? A[N*i + j] : -A[N*i + j];
			tvs += A[N*j + i] > 0 ? A[N*j + i] : -A[N*j + i];
		}
		if (ths > hs)
			hs = ths;
		if (tvs > vs)
			vs = tvs;
	}
	printf("vs = %f, hs = %f\n", vs, hs);
	for (i = 0; i < N; i++)
		for(j = 0; j < N; j++){
			B[N*i + j] = A[N*j + i] / (hs*vs);
		}
	cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, N, N, N,1.0,B,N,A,N,0.0,Z,N);
	// B = At / max h max v
	for (i = 0; i < N; i++)
		for (j = 0; j < N; j++){
			R[N*i + j] = I[N*i + j] - Z[N*i + j];
		}	
	for (i = 0; i < N; i++)
		for (j = 0; j < N; j++)
			Z[N*i + j] = I[N*i + j];

		for (i = 0; i < N; i++)
			for (j = 0; j < N; j++){
				RA[N*i + j] = R[N*i +j];
				RB[N*i + j] = R[N*i +j];
			}
	for (z = 1; z < M; z++){
		for (i = 0; i < N; i++)
			for (j = 0; j < N; j++)
				Z[N*i + j] += R[N*i + j]; 
		cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, N, N, N,1.0,RA,N,RB,N,0.0,R,N);	
		for (i = 0; i < N; i++)
			for (j = 0; j < N; j++)
				RA[N*i + j] = R[N*i + j];	

	}
	cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, N, N, N,1.0,Z,N,B,N,0.0,I,N);
	//cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, N, N, N,1.0,I,N,A,N,0.0,Z,N);
	/*for (i = 0; i < N; i++){
		for (j = 0; j < N; j++){
			printf("%f ", Z[i*N + j]);
		}
		printf("\n");
	}*/
	return 0;
}
